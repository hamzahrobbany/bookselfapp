document.addEventListener("DOMContentLoaded", function () {
  const inputBookForm = document.getElementById("inputBook");
  const incompleteBookshelfList = document.getElementById("incompleteBookshelfList");
  const completeBookshelfList = document.getElementById("completeBookshelfList");
  const searchBookForm = document.getElementById("searchBook");
  const searchBookTitleInput = document.getElementById("searchBookTitle");

  let books = JSON.parse(localStorage.getItem("books")) || [];

  function saveBooksToLocalStorage() {
      localStorage.setItem("books", JSON.stringify(books));
  }

  function displayBooks(filteredBooks) {
      incompleteBookshelfList.innerHTML = "";
      completeBookshelfList.innerHTML = "";

      const booksToDisplay = filteredBooks || books;

      booksToDisplay.forEach((book) => addToBookshelf(book, book.isComplete ? completeBookshelfList : incompleteBookshelfList));
  }

  function addToBookshelf(book, shelfList) {
      const bookItem = document.createElement("article");
      bookItem.classList.add("book_item");
      bookItem.id = book.id;
      bookItem.innerHTML = `
          <h3>${book.title}</h3>
          <p>Penulis: ${book.author}</p>
          <p>Tahun: ${book.year}</p>
          <div class="action">
              <button class="green">${book.isComplete ? "Belum selesai di Baca" : "Selesai dibaca"}</button>
              <button class="red">Hapus buku</button>
              <button class="blue">Edit</button>
          </div>
      `;

      const statusButton = bookItem.querySelector(".green");
      statusButton.addEventListener("click", function () {
          moveBook(book, shelfList.id === "incompleteBookshelfList" ? completeBookshelfList : incompleteBookshelfList);
      });

      const deleteButton = bookItem.querySelector(".red");
      deleteButton.addEventListener("click", function () {
          deleteBook(book);
      });

      const editButton = bookItem.querySelector(".blue");
      editButton.addEventListener("click", function () {
          editBook(book);
      });

      shelfList.appendChild(bookItem);
  }

  function moveBook(book, targetShelf) {
      const currentShelf = book.isComplete ? completeBookshelfList : incompleteBookshelfList;
      currentShelf.removeChild(document.getElementById(book.id));

      book.isComplete = !book.isComplete;
      saveBooksToLocalStorage();
      displayBooks();
  }

  function deleteBook(book) {
    const customDialog = document.getElementById("customDialog");
    const confirmDeleteButton = document.getElementById("confirmDelete");
    const cancelDeleteButton = document.getElementById("cancelDelete");
    const confirmationMessage = document.getElementById("confirmationMessage");

    customDialog.style.display = "block";

    confirmDeleteButton.addEventListener("click", function () {
        books = books.filter((b) => b.id !== book.id);
        saveBooksToLocalStorage();
        displayBooks();
        customDialog.style.display = "none";
        showConfirmationMessage("Buku berhasil dihapus");
    });

    cancelDeleteButton.addEventListener("click", function () {
        customDialog.style.display = "none";
    });
}

function showConfirmationMessage(message) {
    const confirmationMessage = document.getElementById("confirmationMessage");
    confirmationMessage.innerText = message;
    confirmationMessage.style.display = "block";


    setTimeout(function () {
        confirmationMessage.style.display = "none";
    }, 3000);
}



function editBook(book) {
  let newTitle = prompt("Edit judul buku:", book.title);


  if (newTitle === null) {
      return;
  }

  
  const newAuthor = prompt("Edit penulis:", book.author);
  const newYear = prompt("Edit tahun:", book.year);


  if (newTitle.trim() !== "") {
      book.title = newTitle.trim();
  }

  if (newAuthor !== null) {
      book.author = newAuthor;
  }

  if (newYear !== null && !isNaN(newYear)) {
      book.year = parseInt(newYear);
  }

  saveBooksToLocalStorage();
  displayBooks();


  showConfirmationMessage("Buku berhasil diubah");
}

function showConfirmationMessage(message) {
  const confirmationMessage = document.getElementById("confirmationMessage");
  confirmationMessage.innerText = message;
  confirmationMessage.style.display = "block";

 
  setTimeout(function () {
      confirmationMessage.style.display = "none";
  }, 3000);
}




  inputBookForm.addEventListener("submit", function (event) {
      event.preventDefault();

      const inputBookTitle = document.getElementById("inputBookTitle").value;
      const inputBookAuthor = document.getElementById("inputBookAuthor").value;
      const inputBookYear = document.getElementById("inputBookYear").value;
      const inputBookIsComplete = document.getElementById("inputBookIsComplete").checked;

      if (inputBookTitle && inputBookAuthor && inputBookYear) {
          const newBook = {
              id: +new Date(),
              title: inputBookTitle,
              author: inputBookAuthor,
              year: parseInt(inputBookYear),
              isComplete: inputBookIsComplete,
          };

          books.push(newBook);
          saveBooksToLocalStorage();
          displayBooks();
          inputBookForm.reset();
      } else {
          alert("Mohon lengkapi semua kolom formulir");
      }
  });

  searchBookForm.addEventListener("submit", function (event) {
      event.preventDefault();
      const searchTerm = searchBookTitleInput.value.toLowerCase();
      const filteredBooks = books.filter((book) => book.title.toLowerCase().includes(searchTerm));
      displayBooks(filteredBooks);
  });


  displayBooks();
});
